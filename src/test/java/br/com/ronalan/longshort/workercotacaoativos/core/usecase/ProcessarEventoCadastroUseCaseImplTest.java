package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.ronalan.longshort.base.dto.response.ListaErroEnum;
import br.com.ronalan.longshort.base.dto.response.ResponseData;
import br.com.ronalan.longshort.base.dto.response.ResponseDataErro;
import br.com.ronalan.longshort.base.gateway.SalvarGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.ConfiguracaoGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemDeadLetterGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemRetryGateway;
import br.com.ronalan.longshort.workercotacaoativos.template.AtivoRequestTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class ProcessarEventoCadastroUseCaseImplTest {

	@InjectMocks
	private ProcessarEventoCadastroUseCaseImpl processarUseCase;

	@Mock
	private BuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway;

	@Mock
	private ConfiguracaoGateway configuracaoGateway;

	@Mock
	private BuscarCotacoesApiGateway buscarCotacoesApiGateway;

	@Mock
	private SalvarGateway<HistoricoCotacao> salvarGateway;

	@Mock
	private GerarMensagemDeadLetterGateway gerarDeadLetterGateway;

	@Mock
	private GerarMensagemRetryGateway gerarRetryGateway;
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.ronalan.longshort.workercotacaoativos.template");
	}

	@Before
	public void before() {
		Mockito.when(configuracaoGateway.getQuantidadeDiasParaCotacaoNoCadastroAtivo()).thenReturn(300);
	}

	@Test
	public void deve_salvar_carga_cotacao_na_base() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);

		List<HistoricoCotacao> historicos = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			historicos.add(new HistoricoCotacao());
		}

		HistoricoCotacaoResponse response = new HistoricoCotacaoResponse();
		response.setHistoricos(historicos);

		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
				.thenReturn(null);
		Mockito.when(buscarCotacoesApiGateway.buscar(Mockito.eq(request.getCodigo()), Mockito.any(), Mockito.any(),
				Mockito.eq(false))).thenReturn(response);

		processarUseCase.executar(request);

		Mockito.verify(buscarCotacoesApiGateway).buscar(request.getCodigo(),
				LocalDate.now().minus(300, ChronoUnit.DAYS), LocalDate.now(), false);
		Mockito.verify(salvarGateway, Mockito.times(100)).salvar(Mockito.any());
	}

	@Test
	public void nao_deve_salvar_com_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		processarUseCase.executar(request);

		Mockito.verify(gerarDeadLetterGateway).gerar(Mockito.any(AtivoDeadLetterResponse.class));
		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
	}

	@Test
	public void nao_deve_salvar_carga_ja_realizada() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);

		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
				.thenReturn(new HistoricoCotacao());

		processarUseCase.executar(request);

		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
	}

	@Test
	public void deve_gerar_retry_quando_houver_timeout() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		HistoricoCotacaoResponse response = new HistoricoCotacaoResponse();
		ResponseData responseData = new ResponseData();
		responseData.adicionarErro(new ResponseDataErro("TIMEOUT", ListaErroEnum.TIMEOUT));
		response.setResponse(responseData);

		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
				.thenReturn(null);

		Mockito.when(buscarCotacoesApiGateway.buscar(Mockito.eq(request.getCodigo()), Mockito.any(), Mockito.any(),
				Mockito.eq(false))).thenReturn(response);
		
		processarUseCase.executar(request);
		
		Mockito.verify(buscarCotacoesApiGateway, Mockito.times(3)).buscar(request.getCodigo(), 
				LocalDate.now().minus(300, ChronoUnit.DAYS), LocalDate.now(), false);
		Mockito.verify(gerarRetryGateway).gerar(request);
		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
	}
	
	@Test
	public void deve_gerar_deadletter_quando_ocorrer_entidadenaoencontrada() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		HistoricoCotacaoResponse response = new HistoricoCotacaoResponse();
		ResponseData responseData = new ResponseData();
		responseData.adicionarErro(new ResponseDataErro("ENTIDADE NAO ENCONTRADA", ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
		response.setResponse(responseData);

		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
				.thenReturn(null);

		Mockito.when(buscarCotacoesApiGateway.buscar(Mockito.eq(request.getCodigo()), Mockito.any(), Mockito.any(),
				Mockito.eq(false))).thenReturn(response);
		
		processarUseCase.executar(request);
		
		Mockito.verify(buscarCotacoesApiGateway).buscar(request.getCodigo(), 
				LocalDate.now().minus(300, ChronoUnit.DAYS), LocalDate.now(), false);
		Mockito.verify(gerarDeadLetterGateway).gerar(Mockito.any(AtivoDeadLetterResponse.class));
		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
	}

}
