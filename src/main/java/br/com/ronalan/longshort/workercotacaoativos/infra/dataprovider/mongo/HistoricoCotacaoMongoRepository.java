package br.com.ronalan.longshort.workercotacaoativos.infra.dataprovider.mongo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HistoricoCotacaoMongoRepository extends MongoRepository<MongoHistoricoCotacaoEntity, String> {

	Page<MongoHistoricoCotacaoEntity> findByCodigoAtivo(String codigoAtivo, Pageable page);
	
}
