package br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway;

import java.util.List;

import br.com.ronalan.longshort.workercotacaoativos.core.entity.HistoricoCotacao;

/**
 * Busca as cotações de determinado ativo já salvo
 * 
 * @author ronaldo.lanhellas
 */
public interface BuscarCotacoesSalvasPorCodigoAtivoGateway {

	/**
	 * Busca a primeira cotação que for encontrada para determinado ativo
	 * @param codigoAtivo
	 * @return {@link HistoricoCotacao}
	 * @author ronaldo.lanhellas
	 * */
	HistoricoCotacao buscarPrimeiraCotacao(String codigoAtivo);
	
	List<HistoricoCotacao> buscarCotacoes(String codigoAtivo);

}
