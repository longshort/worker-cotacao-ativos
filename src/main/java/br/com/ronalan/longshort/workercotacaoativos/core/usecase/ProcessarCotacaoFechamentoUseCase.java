package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import br.com.ronalan.longshort.base.usecase.BaseUseCase;

/**
 * 
 * @author ronaldo.lanhellas
 */
public interface ProcessarCotacaoFechamentoUseCase extends BaseUseCase<Void, Void> {

}
