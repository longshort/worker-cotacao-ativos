package br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway;

import java.time.LocalDate;

import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;

/**
 * Busca as cotações de determinado ativo através de um API
 * @author ronaldo.lanhellas
 * */
public interface BuscarCotacoesApiGateway {

	/**
	 * Realiza a busca das cotações do ativo através do período informado, código do ativo e 
	 * se é intraday ou não. Se for intraday, o retorno será hora a hora, caso contrário deverá
	 * retornar apenas o valor de fechamento por dia
	 * @param codigoAtivo
	 * @param inicio
	 * @param fim
	 * @param intraday
	 * @return {@link HistoricoCotacaoResponse}
	 * */
	HistoricoCotacaoResponse buscar(String codigoAtivo,	LocalDate inicio, LocalDate fim, boolean full);
	
}
