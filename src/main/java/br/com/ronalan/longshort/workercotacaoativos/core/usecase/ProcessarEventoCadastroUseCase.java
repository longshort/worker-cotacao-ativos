package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import br.com.ronalan.longshort.base.usecase.BaseUseCase;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

/**
 * Processa o evento de cadastro de ativo, buscando todas as cotações deste
 * 
 * @author ronaldo.lanhellas
 */
public interface ProcessarEventoCadastroUseCase extends BaseUseCase<AtivoRequest, Void> {

}
