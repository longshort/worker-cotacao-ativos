package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.base.gateway.SalvarGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.Correlacao;
import br.com.ronalan.longshort.workercotacaoativos.core.entity.CorrelacaoAtivo;
import br.com.ronalan.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.CorrelacaoAtivoRequest;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.CorrelacaoAtivoResponse;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemCorrelacaoGateway;

@Component
public class ProcessarPrimeiraCorrelacaoAtivosUseCaseImpl implements ProcessarPrimeiraCorrelacaoAtivosUseCase {

	private final BuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasGateway;
	private final SalvarGateway<CorrelacaoAtivo> salvarGateway;
	private final GerarMensagemCorrelacaoGateway gerarMensagemCorrelacaoGateway;

	public ProcessarPrimeiraCorrelacaoAtivosUseCaseImpl(
			BuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasGateway,
			SalvarGateway<CorrelacaoAtivo> salvarGateway,
			GerarMensagemCorrelacaoGateway gerarMensagemCorrelacaoGateway) {
		super();
		this.buscarCotacoesSalvasGateway = buscarCotacoesSalvasGateway;
		this.salvarGateway = salvarGateway;
		this.gerarMensagemCorrelacaoGateway = gerarMensagemCorrelacaoGateway;
	}

	@Override
	public Void executar(CorrelacaoAtivoRequest request) {
		List<HistoricoCotacao> histAtivo1 = buscarCotacoesSalvasGateway.buscarCotacoes(request.getAtivo1());
		List<HistoricoCotacao> histAtivo2 = buscarCotacoesSalvasGateway.buscarCotacoes(request.getAtivo2());

		if (!histAtivo1.isEmpty() && !histAtivo2.isEmpty()) {
			calcularCorrelacao(request, histAtivo1, histAtivo2);
		}

		salvarGateway.salvar(new CorrelacaoAtivo(request.getAtivo1(), request.getAtivo2()));
		return null;
	}

	private void calcularCorrelacao(CorrelacaoAtivoRequest request, List<HistoricoCotacao> hist1,
			List<HistoricoCotacao> hist2) {
		double valorCorrelacao = Correlacao.computePersonsCorrelationCoefficent(hist1.size(),
				hist1.stream().map(HistoricoCotacao::getValor).collect(Collectors.toList()),
				hist2.stream().map(HistoricoCotacao::getValor).collect(Collectors.toList()));
		gerarMensagemCorrelacaoGateway
				.gerar(new CorrelacaoAtivoResponse(request.getAtivo1(), request.getAtivo2(), valorCorrelacao));
	}

}
