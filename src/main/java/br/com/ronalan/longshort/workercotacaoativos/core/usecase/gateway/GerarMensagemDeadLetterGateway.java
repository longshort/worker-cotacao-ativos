package br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;

/**
 * Gera mensagem de deadletter
 * @author ronaldo.lanhellas
 * */
public interface GerarMensagemDeadLetterGateway {

	/**
	 * Gera a mensagem de deadletter
	 * @param response
	 * */
	void gerar(AtivoDeadLetterResponse response);
	
}
