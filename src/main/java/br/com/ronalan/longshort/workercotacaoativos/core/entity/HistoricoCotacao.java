package br.com.ronalan.longshort.workercotacaoativos.core.entity;

import java.time.LocalDate;
import java.time.LocalTime;

import br.com.ronalan.longshort.base.entity.BaseEntity;

/**
 * Responsável por manter todo o histórico de cotações de um ativo
 * 
 * @author ronaldo.lanhellas
 */
public class HistoricoCotacao extends BaseEntity {

	private String codigoAtivo;
	private Double valor;
	private LocalDate data;
	private LocalTime hora;
	
	public HistoricoCotacao() {
	}

	public HistoricoCotacao(String codigoAtivo, Double valor, LocalDate data) {
		this.codigoAtivo = codigoAtivo;
		this.valor = valor;
		this.data = data;
	}
	
	public HistoricoCotacao(String id, String codigoAtivo, Double valor, LocalDate data) {
		this.id = id;
		this.codigoAtivo = codigoAtivo;
		this.valor = valor;
		this.data = data;
	}
	
	public String getCodigoAtivo() {
		return codigoAtivo;
	}

	public void setCodigoAtivo(String codigoAtivo) {
		this.codigoAtivo = codigoAtivo;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "HistoricoCotacao [codigoAtivo=" + codigoAtivo + ", valor=" + valor + ", data=" + data + ", hora=" + hora
				+ "]";
	}
	
}
