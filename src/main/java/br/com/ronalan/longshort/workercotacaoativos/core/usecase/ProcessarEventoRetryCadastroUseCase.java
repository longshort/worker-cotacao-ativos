package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import br.com.ronalan.longshort.base.usecase.BaseUseCase;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

/**
 * Processa o evento de retry de cadastro de ativo
 * 
 * @author ronaldo.lanhellas
 */
public interface ProcessarEventoRetryCadastroUseCase extends BaseUseCase<AtivoRequest, Void> {

}
