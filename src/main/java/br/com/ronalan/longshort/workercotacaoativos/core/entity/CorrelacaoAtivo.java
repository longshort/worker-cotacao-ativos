package br.com.ronalan.longshort.workercotacaoativos.core.entity;

import br.com.ronalan.longshort.base.entity.BaseEntity;

public class CorrelacaoAtivo extends BaseEntity {

	private String ativo1;
	private String ativo2;
	
	public CorrelacaoAtivo(String ativo1, String ativo2) {
		super();
		this.ativo1 = ativo1;
		this.ativo2 = ativo2;
	}

	public String getAtivo1() {
		return ativo1;
	}
	public void setAtivo1(String ativo1) {
		this.ativo1 = ativo1;
	}
	public String getAtivo2() {
		return ativo2;
	}
	public void setAtivo2(String ativo2) {
		this.ativo2 = ativo2;
	}

	

}
