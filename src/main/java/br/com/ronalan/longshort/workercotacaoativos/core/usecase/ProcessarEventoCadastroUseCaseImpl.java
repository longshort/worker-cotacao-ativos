package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.base.dto.response.ListaErroEnum;
import br.com.ronalan.longshort.base.gateway.SalvarGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.ConfiguracaoGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemDeadLetterGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemRetryGateway;

/**
 * {@inheritDoc}
 */
@Component
public class ProcessarEventoCadastroUseCaseImpl extends ProcessarEventoCadastroUseCaseBase
		implements ProcessarEventoCadastroUseCase {

	private final GerarMensagemRetryGateway gerarMensagemRetryGateway;

	public ProcessarEventoCadastroUseCaseImpl(GerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway,
			BuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway,
			SalvarGateway<HistoricoCotacao> salvarGateway, BuscarCotacoesApiGateway buscarCotacoesGateway,
			ConfiguracaoGateway configuracaoGateway, GerarMensagemRetryGateway gerarMensagemRetryGateway) {
		super(gerarMensagemDeadLetterGateway, buscarCotacoesSalvasPorCodigoAtivoGateway, salvarGateway,
				buscarCotacoesGateway, configuracaoGateway);
		this.gerarMensagemRetryGateway = gerarMensagemRetryGateway;
	}

	@Override
	protected HistoricoCotacaoResponse buscarCotacoes(AtivoRequest request, LocalDate inicio, LocalDate fim,
			int tentativas) {
		HistoricoCotacaoResponse responseCotacao = buscarCotacoesGateway.buscar(request.getCodigo(), inicio, fim,
				true);
		if (!responseCotacao.getResponse().getErros().isEmpty()) {

			if (responseCotacao.getResponse().getErros().stream().filter(e -> ListaErroEnum.TIMEOUT.equals(e.getTipo()))
					.count() > 0) {
				if (tentativas < 2) {
					return buscarCotacoes(request, inicio, fim, ++tentativas);
				} else {
					logger.info("Gerando mensagem de Retry do ativo {}", request.getCodigo());
					gerarMensagemRetryGateway.gerar(request);
				}
			} else {
				logger.info("Gerando mensagem de DeadLetter do ativo {}", request.getCodigo());
				gerarMensagemDeadLetterGateway
						.gerar(new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao()));
			}
		}

		return responseCotacao;
	}
}
