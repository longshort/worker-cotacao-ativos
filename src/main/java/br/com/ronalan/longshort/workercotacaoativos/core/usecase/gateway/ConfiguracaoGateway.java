package br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway;

public interface ConfiguracaoGateway {

	/**
	 * Retorna a quantidade de dias para busca de cotações no evento de cadastro de ativos
	 * @return {@link Integer}
	 * */
	Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo();
	
}
