package br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway;

import java.util.List;

/**
 * @author ronaldo.lanhellas
 */
public interface BuscarCodigoCotacoesGateway {

	List<String> buscarCodigos();

}
