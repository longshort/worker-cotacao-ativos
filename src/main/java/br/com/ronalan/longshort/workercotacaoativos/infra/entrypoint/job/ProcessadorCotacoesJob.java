package br.com.ronalan.longshort.workercotacaoativos.infra.entrypoint.job;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.workercotacaoativos.core.usecase.ProcessarCotacaoFechamentoUseCase;

@Component
public class ProcessadorCotacoesJob {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final ProcessarCotacaoFechamentoUseCase processarCotacaoFechamentoUseCase;
	
	public ProcessadorCotacoesJob(ProcessarCotacaoFechamentoUseCase processarCotacaoFechamentoUseCase) {
		this.processarCotacaoFechamentoUseCase = processarCotacaoFechamentoUseCase;
	}

	@Scheduled(cron = "0 0/2 * * * *")
	private void executar() {
		logger.info("Executando job às: {}",LocalDateTime.now());	
		processarCotacaoFechamentoUseCase.executar(null);
	}
	
}
