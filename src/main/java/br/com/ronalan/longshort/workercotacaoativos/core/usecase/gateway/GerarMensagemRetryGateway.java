package br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

/**
 * Gera mensagem de retry
 * @author ronaldo.lanhellas
 * */
public interface GerarMensagemRetryGateway {

	/**
	 * Gera a mensagem de retry
	 * @param response
	 * */
	void gerar(AtivoRequest request);
	
}
