package br.com.ronalan.longshort.workercotacaoativos.infra.dataprovider.mongo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.base.gateway.SalvarGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCodigoCotacoesGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorCodigoAtivoGateway;

@Component
public class MongoDataProvider implements BuscarCotacoesSalvasPorCodigoAtivoGateway, SalvarGateway<HistoricoCotacao>,
										  BuscarCodigoCotacoesGateway{

	private final HistoricoCotacaoMongoRepository mongoRepository;
	private final MongoTemplate mongoTemplate;

	public MongoDataProvider(HistoricoCotacaoMongoRepository mongoRepository, MongoTemplate mongoTemplate) {
		this.mongoRepository = mongoRepository;
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public HistoricoCotacao buscarPrimeiraCotacao(String codigoAtivo) {
		Page<MongoHistoricoCotacaoEntity> pageHist = mongoRepository.findByCodigoAtivo(codigoAtivo,
				PageRequest.of(0, 1));
		if (pageHist.getNumberOfElements() > 0) {
			MongoHistoricoCotacaoEntity mongoEntity = pageHist.getContent().get(0);
			return new HistoricoCotacao(mongoEntity.getId(), mongoEntity.getCodigoAtivo(), mongoEntity.getValor(),
					mongoEntity.getData());
		} else {
			return null;
		}
	}

	@Override
	public HistoricoCotacao salvar(HistoricoCotacao hist) {
		mongoRepository.save(
				new MongoHistoricoCotacaoEntity(hist.getId(), hist.getCodigoAtivo(), hist.getValor(), hist.getData()));
		return hist;
	}

	@Override
	public List<String> buscarCodigos() {
		return mongoTemplate.findDistinct("codigoAtivo", MongoHistoricoCotacaoEntity.class, String.class);
	}

}
