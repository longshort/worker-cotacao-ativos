package br.com.ronalan.longshort.workercotacaoativos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableKafka
@EnableFeignClients
@EnableScheduling
public class WorkerCotacaoAtivos1Application {

	public static void main(String[] args) {
		SpringApplication.run(WorkerCotacaoAtivos1Application.class, args);
	}

}
