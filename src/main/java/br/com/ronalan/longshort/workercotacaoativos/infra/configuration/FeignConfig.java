package br.com.ronalan.longshort.workercotacaoativos.infra.configuration;

import org.springframework.context.annotation.Bean;

import feign.Logger;

public class FeignConfig {

	@Bean
	public Logger.Level getLevel(){
		return Logger.Level.FULL;
	}
	
}
