package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.ronalan.longshort.base.gateway.SalvarGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCodigoCotacoesGateway;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;

@Component
public class ProcessarCotacaoFechamentoUseCaseImpl implements ProcessarCotacaoFechamentoUseCase {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final BuscarCotacoesApiGateway buscarCotacoesApiGateyway;
	private final BuscarCodigoCotacoesGateway buscarCodigoCotacoesGateway;
	private final SalvarGateway<HistoricoCotacao> salvarGateway;

	public ProcessarCotacaoFechamentoUseCaseImpl(BuscarCotacoesApiGateway buscarCotacoesApiGateyway,
			BuscarCodigoCotacoesGateway buscarCodigoCotacoesGateway, SalvarGateway<HistoricoCotacao> salvarGateway) {
		this.buscarCotacoesApiGateyway = buscarCotacoesApiGateyway;
		this.buscarCodigoCotacoesGateway = buscarCodigoCotacoesGateway;
		this.salvarGateway = salvarGateway;
	}

	@Override
	public Void executar(Void arg0) {
		logger.info("Iniciando coleta das cotações de fechamento...");
		List<String> codigos = buscarCodigoCotacoesGateway.buscarCodigos();
		LocalDate hoje = LocalDate.now();
		codigos.forEach(c -> {
			HistoricoCotacaoResponse response = buscarCotacoesApiGateyway.buscar(c, hoje, hoje, false);
			if (!response.getHistoricos().isEmpty()) {
				HistoricoCotacao ct = response.getHistoricos().get(0);
				logger.info("Salvando cotação do ativo {} na data/hora {}/{}", c, ct.getData(), ct.getHora());
				salvarGateway.salvar(ct);
			}
		});

		return null;
	}

}
