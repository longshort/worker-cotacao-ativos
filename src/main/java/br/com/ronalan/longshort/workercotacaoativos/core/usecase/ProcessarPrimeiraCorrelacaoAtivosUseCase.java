package br.com.ronalan.longshort.workercotacaoativos.core.usecase;

import br.com.ronalan.longshort.base.usecase.BaseUseCase;
import br.com.ronalan.longshort.workercotacaoativos.core.usecase.dto.CorrelacaoAtivoRequest;

/**
 * 
 * @author ronaldo.lanhellas
 */
public interface ProcessarPrimeiraCorrelacaoAtivosUseCase 
		extends BaseUseCase<CorrelacaoAtivoRequest, Void> {

}
